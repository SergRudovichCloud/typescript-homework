function getUrl<T>(url: string): Promise<T> {
  return fetch(url)
    .then(response => {
      if (!response.ok) {
        throw new Error(response.statusText)
      }
      return response.json()
    })
    .then(data => {
      return data
    })
    .catch((error: Error) => {
      throw error
    })
}

export { getUrl }