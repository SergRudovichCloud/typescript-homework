class FavoriteList {
    constructor() {
        if (localStorage.getItem("binary_movies") == null)
            localStorage.setItem("binary_movies", JSON.stringify([]));
    }

    _getItem() {
        return JSON.parse(localStorage.getItem("binary_movies") as string);
    }

    _setItem(): void {
        localStorage.setItem("binary_movies", JSON.stringify(this.favoriteArray));
    }

    favoriteArray: Array<string> = [];

    add(item: string) {
        this.favoriteArray = this._getItem();
        this.favoriteArray.push(item);
        this._setItem();
    }

    remove(item: string) {
        this.favoriteArray = this._getItem();
        this.favoriteArray.splice(this.favoriteArray.indexOf(item), 1);
        this._setItem();
    }

    has(item: string) {
        this.favoriteArray = this._getItem();
        let result = false;
        for (let i = 0; i < this.favoriteArray.length; i++)
            if (this.favoriteArray[i] == item as string) result = true;
        return result;
    }

    getAll() {
        return this.favoriteArray = this._getItem();
    }
}

const favoritList = new FavoriteList();

export { favoritList }