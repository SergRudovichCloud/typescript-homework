function mapper(movies: MoviesResult | MoviesDetails): Movie {
    const movie = <Movie>{};
        movie.backdrop_path = movies.backdrop_path;
        movie.id = movies.id;
        movie.overview = movies.overview;
        movie.poster_path = movies.poster_path;
        movie.release_date = movies.release_date;
    return movie;
}

export { mapper }