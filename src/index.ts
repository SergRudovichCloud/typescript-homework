import { showFilms, clearFilms, clearFavorite, viewFavorite } from './services/services'
import {
  POPULAR_MOVIES,
  TOP_RATED_MOVIES,
  UPCOMING_MOVIES,
  QUERY,
  favorite,
  loadMoreBtn,
  popularBtn,
  upcomingBtn,
  topRatedBtn,
  searchInput,
  searchBtn,
  favoriteBtn,
  closeFavoriteBtn
} from './constants'


export async function render(): Promise<void> {

  let page = 1;
  let currenrUrl = POPULAR_MOVIES;
  let url = currenrUrl + '1';

  function view(apiUrl: string): void {
    clearFilms();
    currenrUrl = apiUrl;
    url = currenrUrl + '1';
    showFilms(url);
    searchInput.value = '';
  }

  function updateCards(): void {
    if (popularBtn.checked) view(POPULAR_MOVIES);
    if (upcomingBtn.checked) view(UPCOMING_MOVIES);
    if (topRatedBtn.checked) view(TOP_RATED_MOVIES);
  }

  popularBtn.onchange = () => view(POPULAR_MOVIES);
  upcomingBtn.onchange = () => view(UPCOMING_MOVIES);
  topRatedBtn.onchange = () => view(TOP_RATED_MOVIES);

  searchBtn.onclick = () => {
    try {
      clearFilms();
      page = 1;
      url = QUERY + searchInput.value + '&page=1';
      showFilms(url);
    }
    catch (e) {
      console.log(e);
    }
  };

  loadMoreBtn.onclick = () => {
    page++;
    url = currenrUrl + page as string;
    showFilms(url);
  };

  favoriteBtn.onclick = () => {
    clearFavorite();
    viewFavorite();
  };

  closeFavoriteBtn.onclick = updateCards;

  favorite.onblur = updateCards;

  clearFavorite();
  viewFavorite();
  view(POPULAR_MOVIES);

}