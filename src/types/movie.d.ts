interface Movie {
    poster_path: string | null
    overview: string
    release_date: string
    id: number
    backdrop_path: string | null
}