const loadMoreBtn = <HTMLButtonElement>document.getElementById('load-more');
const popularBtn = <HTMLInputElement>document.getElementById('popular');
const upcomingBtn = <HTMLInputElement>document.getElementById('upcoming');
const topRatedBtn = <HTMLInputElement>document.getElementById('top_rated');
const searchInput = <HTMLInputElement>document.getElementById('search');
const searchBtn = <HTMLButtonElement>document.getElementById('submit');
const favoriteBtn = <HTMLButtonElement>document.getElementById('favorit');
const closeFavoriteBtn = <HTMLButtonElement>document.getElementById('closeFavorite');
const randomMovieDescription = <HTMLElement>document.getElementById('random-movie-description');
const randomMovieName = <HTMLElement>document.getElementById('random-movie-name');
const randomImg = <HTMLDivElement>document.getElementById('random-movie');
const favorite = <HTMLDivElement>document.getElementById('offcanvasRight');

const CARDS_CONTAINER = <HTMLDivElement>document.getElementById('film-container');
const FAVORITE_CONTAINER = <HTMLDivElement>document.getElementById('favorite-movies');

const API_KEY = 'baaa77deb235696fd5f43ecc1420619b'
const IMG_URL = 'https://image.tmdb.org/t/p/original//'

const POPULAR_MOVIES = `https://api.themoviedb.org/3/movie/popular?api_key=${API_KEY}&language=en-US&page=`
const TOP_RATED_MOVIES = `https://api.themoviedb.org/3/movie/top_rated?api_key=${API_KEY}&language=en-US&page=`
const UPCOMING_MOVIES = `https://api.themoviedb.org/3/movie/upcoming?api_key=${API_KEY}&language=en-US&page=`
const QUERY = `https://api.themoviedb.org/3/search/movie?api_key=${API_KEY}&language=en-US&query=`
const MOVIES_DETAILS = `https://api.themoviedb.org/3/movie/?api_key=${API_KEY}&language=en-US`

export {
    POPULAR_MOVIES,
    TOP_RATED_MOVIES,
    UPCOMING_MOVIES,
    MOVIES_DETAILS,
    QUERY,
    IMG_URL,
    CARDS_CONTAINER,
    FAVORITE_CONTAINER,
    loadMoreBtn,
    popularBtn,
    upcomingBtn,
    topRatedBtn,
    searchInput,
    searchBtn,
    favoriteBtn,
    closeFavoriteBtn,
    randomMovieDescription,
    randomMovieName,
    randomImg,
    favorite
}