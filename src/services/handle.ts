import { favoritList } from '../helpers/helpers'
import { clearFavorite, viewFavorite } from './services'

function handleFavoriteClick(event: Event): void {
    const film = event.currentTarget as any;
    const isNotLike = film.getAttribute("fill-opacity") as number;

    if (isNotLike == 1) {
        if (film.id[0] == '!') {
            favoritList.remove(film.id.slice(1));
        } else {
            favoritList.remove(film.id);
        }

        film.setAttribute("fill-opacity", 0)
    } else {
        favoritList.add(film.id);
        film.setAttribute("fill-opacity", 1)
    }

    clearFavorite();
    viewFavorite();

}


export { handleFavoriteClick }