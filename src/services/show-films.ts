import { appendCard, appendFavorite, randomMovie } from './services'
import { getUrl, mapper } from '../helpers/helpers'

function showFilms(url: string): void {
    getUrl<Movies>(url)
        .then((movies) => {
            movies.results.forEach(el => appendCard(mapper(el)));
            randomMovie(movies);
        })
        .catch(error => {
            console.log(error)
        })
}

function showFavorite(url: string): void {
    getUrl<MoviesDetails>(url)
        .then((data) => {
            appendFavorite(mapper(data));
        })
        .catch(error => {
            console.log(error)
        })
}

export { showFilms, showFavorite }
