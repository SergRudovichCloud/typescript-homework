import { IMG_URL, randomMovieDescription, randomMovieName, randomImg } from '../constants'

function randomMovie(movies: Movies): void {
    const i = randomI();
    randomMovieName.innerHTML = movies.results[i].title;
    randomMovieDescription.innerHTML = movies.results[i].overview;
    randomImg.style.backgroundImage = `url(${IMG_URL + movies.results[i].backdrop_path})`;
}

function randomI(): number {
    return Math.round(0.5 + Math.random() * 20);
}

export { randomMovie }