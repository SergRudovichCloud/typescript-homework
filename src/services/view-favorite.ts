import { favoritList } from '../helpers/helpers'
import { showFavorite } from '../services/services'
import { MOVIES_DETAILS } from '../constants'

function viewFavorite(): void {
    const favorit = favoritList.getAll();
    for (let i = 0; i < favorit.length; i++) {
      const id = favorit[i];
      const movieUrl = MOVIES_DETAILS.slice(0, 35) + id + MOVIES_DETAILS.slice(35);
      showFavorite(movieUrl);
    }
  }

  export { viewFavorite }