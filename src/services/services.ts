import { appendCard } from './append-card'
import { clearFilms, clearFavorite } from './clear-films'
import { showFilms, showFavorite } from './show-films'
import { handleFavoriteClick } from './handle'
import { appendFavorite } from './append-favorite'
import { randomMovie } from './random-movie'
import { viewFavorite } from './view-favorite'

export {
    appendCard,
    clearFilms,
    showFilms,
    handleFavoriteClick,
    clearFavorite,
    appendFavorite,
    showFavorite,
    randomMovie,
    viewFavorite
}