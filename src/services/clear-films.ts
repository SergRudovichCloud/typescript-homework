import { CARDS_CONTAINER, FAVORITE_CONTAINER } from '../constants'

function clearFilms(): void {
    CARDS_CONTAINER.innerHTML = '';
}

function clearFavorite(): void {
    FAVORITE_CONTAINER.innerHTML = '';
}

export { clearFilms, clearFavorite }