import { CARDS_CONTAINER, IMG_URL } from '../constants'
import { handleFavoriteClick } from './services'
import { favoritList } from '../helpers/helpers'
import { cardContent } from '../helpers/helpers'

function appendCard(movie: Movie): void {
    const id = (movie.id as unknown) as string;
    const card = document.createElement('div');
    card.classList.add('col-lg-3', 'col-md-4', 'col-12', 'p-2');
    card.innerHTML = cardContent(IMG_URL + movie.poster_path, movie.overview, movie.release_date, (movie.id as unknown) as string)
    CARDS_CONTAINER?.appendChild(card);
    const likeBtn = <HTMLElement>document.getElementById(id);
    likeBtn.addEventListener('click', handleFavoriteClick);
    if (!favoritList.has(id)) likeBtn.setAttribute("fill-opacity", "0")
}

export { appendCard }