import { FAVORITE_CONTAINER, IMG_URL } from '../constants'
import { handleFavoriteClick } from './services'
import { cardContent } from '../helpers/helpers'

function appendFavorite(data: Movie): void {
    const id = (data.id as unknown) as string;
    const card = document.createElement('div');
    card.classList.add('col-12', 'p-2');
    card.innerHTML = cardContent(IMG_URL + data.poster_path, data.overview, data.release_date, '!' + id)
    FAVORITE_CONTAINER?.appendChild(card);
    const likeBtn = <HTMLElement>document.getElementById('!' + id);
    likeBtn.addEventListener('click', handleFavoriteClick);
}

export { appendFavorite }